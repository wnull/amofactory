<?php

declare(strict_types=1);

use AmoTestJob\AmoFactory;
use League\OAuth2\Client\Token\AccessTokenInterface;

require_once __DIR__ . '/bootstrap.php';

$accessToken = getToken();

$apiClient
    ->setAccessToken($accessToken)
    ->onAccessTokenRefresh(
        function (AccessTokenInterface $accessToken, string $baseDomain) {
            saveToken(
                [
                    'accessToken' => $accessToken->getToken(),
                    'refreshToken' => $accessToken->getRefreshToken(),
                    'expires' => $accessToken->getExpires(),
                    'baseDomain' => $baseDomain,
                ]
            );
        }
    );


$obj = new AmoFactory($apiClient, 50);

$obj->addCustomFields('test-field2', ['test-value2', 'test-value-4']);

$obj->addLeadsWithCompaniesAndContacts(1000);
