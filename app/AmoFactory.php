<?php

declare(strict_types=1);

namespace AmoTestJob;

use AmoCRM\Client\AmoCRMApiClient;
use AmoCRM\Helpers\EntityTypesInterface;
use AmoCRM\Exceptions\AmoCRMApiException;
use AmoCRM\Collections\ContactsCollection;
use AmoCRM\Collections\Leads\LeadsCollection;
use AmoCRM\Collections\CustomFields\CustomFieldEnumsCollection;
use AmoCRM\Models\LeadModel;
use AmoCRM\Models\ContactModel;
use AmoCRM\Models\CompanyModel;
use AmoCRM\Models\CustomFields\EnumModel;
use AmoCRM\Models\CustomFields\MultiselectCustomFieldModel;
use AmoCRM\Models\CustomFieldsValues\MultiselectCustomFieldValuesModel;
use AmoCRM\Models\CustomFieldsValues\ValueCollections\MultiselectCustomFieldValueCollection;
use AmoCRM\Models\CustomFieldsValues\ValueModels\MultiselectCustomFieldValueModel;
use AmoCRM\Collections\CustomFieldsValuesCollection;
use AmoCRM\Models\CustomFields\CustomFieldModel;

class AmoFactory
{
    public AmoCRMApiClient $client;
    protected int $leadsComplexRequestLimit;

    private int $fieldId;
    /**
     * AmoFactory constructor.
     *
     * @param AmoCRMApiClient $apiClient
     * @param integer $leadsComplexRequestLimit
     */
    public function __construct(AmoCRMApiClient $apiClient, $leadsComplexRequestLimit = 50)
    {
        $this->client = $apiClient;
        $this->leadsComplexRequestLimit = $leadsComplexRequestLimit;
    }

    /**
     * @param string $name
     * @param array $variants
     * @return MultiselectCustomFieldModel
     */
    public function addCustomFields(string $name, array $variants): CustomFieldModel
    {
        $customFieldsService = $this->client->customFields(EntityTypesInterface::LEADS);
        $multiselectCustomFieldModel = new MultiselectCustomFieldModel();

        $makit = [];
        foreach ($variants as $key => $variant) {
            $makit[] = (new EnumModel())
                ->setValue($variant)
                ->setSort($key);
        }

        $enums = (new CustomFieldEnumsCollection())
            ->make($makit);

        $multiselectCustomFieldModel->setEnums($enums)->setName($name);

        $result = $customFieldsService->addOne($multiselectCustomFieldModel);
        $this->fieldId = $result->getId();

        return $result;
    }

    /**
     * @param integer $count
     * @return void
     */
    public function addLeadsWithCompaniesAndContacts(int $count): void
    {
        $leadsCollection = new LeadsCollection();

        for ($i = 1; $i <= $count; $i++) 
        {
            $contactsCollection = new ContactsCollection();
            $contactModel = new ContactModel();
            $contactModel
                ->setFirstName("Контакт")
                ->setLastName("№$i");
            
            $contactsCollection->add($contactModel);
            
            $companyModel = new CompanyModel();
            $companyModel
                ->setName("Компания №$i");
            
                        
            $leadModel = new LeadModel();
            
            $lead = $leadModel
                ->setName("Сделка №$i")
                ->setPrice(mt_rand(100, 50000))
                ->setContacts($contactsCollection)
                ->setCompany($companyModel);
    
            $leadsCollection->add($lead);

            if ($i % 50 === 0) {
                $this->complex($leadsCollection);
                $leadsCollection = new LeadsCollection();
            }

            echo "Add to collection: Компания №$i, Контакт #$i\n";
        }

        if ($leadsCollection->count()) {
            $this->complex($leadsCollection);
        }
    }

    /**
     * @param LeadsCollection $collection
     * @return void
     */
    public function updateMultiselectValues(LeadsCollection $collection): void
    {
        $leads = $this->client->leads();
   
        foreach ($collection as $lead)
        {
            $cfvc = new CustomFieldsValuesCollection();

            $multi = new MultiselectCustomFieldValueCollection();

            $multiModelVal = new MultiselectCustomFieldValueModel();
            $multiModelVal->setValue('test-value2');

            $multiModelValues = new MultiselectCustomFieldValuesModel();
            $multiModelValues
                ->setFieldId($this->fieldId)
                ->setValues(
                    $multi->add($multiModelVal)
                );

            $cfvc->add(
                $multiModelValues
            );

            $lead->setCustomFieldsValues($cfvc);
        }
        
        $leads->update($collection);
    }

    /**
     * @param LeadsCollection $chunk
     */
    private function complex(LeadsCollection $chunk): void
    {
        try {
            echo "Send complex ".$chunk->count()."\n";
            $cmplx = $this->client->leads()->addComplex($chunk);

            $this->updateMultiselectValues($cmplx);
        } 
        catch (AmoCRMApiException $e) {
            echo $e->getMessage();
            var_dump($e);
        }
    }
}
