<?php

declare(strict_types=1);

use AmoCRM\Client\AmoCRMApiClient;

require __DIR__ . '/vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$apiClient = new AmoCRMApiClient(
    $_ENV['CLIENT_ID'],
    $_ENV['SECRET_KEY'], 
    $_ENV['REDIRECT_URI']
);

$apiClient->setAccountBaseDomain($_ENV['SUBDOMAIN']);

include_once __DIR__ . '/getToken.php';
